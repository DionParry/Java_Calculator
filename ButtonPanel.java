import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;


/**
 * Title: Java Calculator - Lab6
 * @author Dion
 * 13/11/2016
 */

/**
 * A panel with calculator buttons and a result display.
 */
public class ButtonPanel extends JPanel {

    private JButton display;
    private JPanel panel;
    private boolean start;
    Calculator calc = new Calculator();

    /**
     * Creates the panel methods and display button
     */
    public ButtonPanel() {
        setLayout(new BorderLayout());
        start = true;

        // add the display
        display = new JButton("0");
        display.setEnabled(false);
        display.setHorizontalAlignment(SwingConstants.RIGHT); //moves text to white
        display.setBackground(Color.WHITE);//sets background colour of display white
        add(display, BorderLayout.NORTH);
        createUpperPanel();
        createControlPanel();

    }

    /**
     * sets buttons to the Upper Center
     */
    public void createUpperPanel() {
        ActionListener function = new FunctionAction();

        panel = new JPanel();
        panel.setLayout(new GridLayout(1, 4, 5, 5));
        panel.setBorder(new EmptyBorder(4, 0, 0, 4));

        JLabel space = new JLabel();
        panel.add(space);

        addButton("BS", function, Color.RED);
        addButton("CE", function, Color.RED);
        addButton("C", function, Color.RED);
        add(panel, BorderLayout.CENTER);

    }

    /**
     * sets buttons to the South panel
     */
    public void createControlPanel() {

        ActionListener insert = new InsertAction();
        ActionListener command = new CommandAction();
        ActionListener function = new FunctionAction();

        // add the buttons in a 4 x 4 grid
        panel = new JPanel();
        panel.setLayout(new GridLayout(4, 6, 4, 5));
        panel.setBorder(new EmptyBorder(4, 4, 4, 4));

        addButton("MC", function, Color.RED);
        addButton("7", insert, Color.BLUE);
        addButton("8", insert, Color.BLUE);
        addButton("9", insert, Color.BLUE);

        addButton("/", command, Color.RED);
        addButton("\u221A", command, Color.BLUE); //square root
        addButton("MR", function, Color.RED);
        addButton("4", insert, Color.BLUE);
        addButton("5", insert, Color.BLUE);
        addButton("6", insert, Color.BLUE);
        addButton("*", command, Color.RED);
        addButton("%", command, Color.BLUE); //percentage
        addButton("MS", function, Color.RED);
        addButton("1", insert, Color.BLUE);
        addButton("2", insert, Color.BLUE);
        addButton("3", insert, Color.BLUE);
        addButton("-", command, Color.RED);
        addButton("1/x", command, Color.BLUE);
        addButton("M+", function, Color.RED);
        addButton("0", insert, Color.BLUE);
        addButton("+/-", command, Color.BLUE);
        addButton(".", insert, Color.BLUE);
        addButton("=", command, Color.RED);
        addButton("+", command, Color.RED);
        add(panel, BorderLayout.SOUTH);
    }

    /**
     * Adds a button to the center panel.
     *
     * @param label the button label
     * @param listener the button listener
     */
    private void addButton(String label, ActionListener listener, Color color) {
        JButton button = new JButton(label);
        button.addActionListener(listener);
        button.setForeground(color);
        panel.add(button);
    }

    /**
     * This action inserts the button action string to the end of the display
     * text.
     */
    private class InsertAction implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            String input = event.getActionCommand();
            if (start == true) {
                display.setText("");
                start = false;
            }
            display.setText(display.getText() + input);
        }
    }

    /**
     * This action executes the command that the button action string denotes.
     */
    private class CommandAction implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();

            if (start == true) {
                if (command.equals("-")) {
                    display.setText(command);
                    start = false;
                } else {
                    calc.lastOperator = command;
                }
            } else {
                calc.calculate(Double.parseDouble(display.getText()));
                calc.lastOperator = command;
                display.setText(calc.result + "");
                start = true;
            }
        }
    }

    
    /**
     * This action inserts the button to the display.
     */
    private class FunctionAction implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            String function = event.getActionCommand();
            
            display.setText(function);
            System.out.println(function);
            
        }
    }
}
