import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


/**
 * Title: Java Calculator - Lab6
 * @author Dion
 * 13/11/2016
 */
public class MenuBar extends JMenuBar implements ActionListener {

    JMenuBar menuBar;
    JMenu edit, view, help;
    JMenuItem copy, paste;
    JMenuItem standard, scientific, digitGrouping;
    JMenuItem helpTopics, aboutCalculator;
    Calculator calc = new Calculator();

    public MenuBar() {

        menuBar = new JMenuBar();
        add(menuBar);

        //Build the first handling on the menu bar.
        edit = new JMenu("Edit");
        menuBar.add(edit);
        view = new JMenu("View");
        menuBar.add(view);
        help = new JMenu("Help");
        menuBar.add(help);

        //Add a sub-menu to edit, first heading on the menu bar.
        copy = new JMenuItem("Copy");
        copy.addActionListener(this);
        edit.add(copy);
        paste = new JMenuItem("Paste");
        paste.addActionListener(this);
        edit.add(paste);

        //Add a sub-menu to view
        standard = new JMenuItem("Standard");
        view.add(standard);
        scientific = new JMenuItem("Scientific");
        view.add(scientific);
        view.addSeparator();
        digitGrouping = new JMenuItem("Digit Grouping");
        view.add(digitGrouping);

        //Add a sub-menu to help
        helpTopics = new JMenuItem("Help Topics");
        helpTopics.addActionListener(this);
        help.add(helpTopics);
        help.addSeparator();
        aboutCalculator = new JMenuItem("About Calculator");
        aboutCalculator.addActionListener(this); //cheeky actionListener
        help.add(aboutCalculator);

    }

    /**
     * Find and opens a local file
     */
    public void openFile() {
        String filePath = new File("").getAbsolutePath();
        File readme = new File(filePath + "/readme.txt");
        //first check if Desktop is supported by Platform or not
        if (!Desktop.isDesktopSupported()) {
            System.out.println("Desktop is not supported");
            return;
        }

        Desktop desktop = Desktop.getDesktop();
        if (readme.exists()) {
            try {
                desktop.open(readme);
            } catch (IOException ex) {
                Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(aboutCalculator)) {
            JOptionPane.showMessageDialog(null, "Calculator version: 151.11.54", "About", 1);
        }
        if (e.getSource().equals(helpTopics)) {
            openFile();
        }
        if (e.getSource().equals(copy)) {
            
            calc.getResult();
            System.out.println(calc.result);

            System.out.println("win");
        }
        if (e.getSource().equals(paste)) {
            System.out.println(copy);
        }
    }
}
