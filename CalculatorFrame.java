/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.JFrame;

/**
 * Title: Java Calculator - Lab6
 * @author Dion 
 * 13/11/2016
 */
public class CalculatorFrame extends JFrame {

    public CalculatorFrame() {

        setTitle("Calculator");
        ButtonPanel panel = new ButtonPanel();
        add(panel);
        pack();
    }
}
