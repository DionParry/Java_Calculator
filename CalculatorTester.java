/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Title: Java Calculator - Lab6
 * @author Dion
 * 13/11/2016
 */
public class CalculatorTester {

    /**
     * Calculator Class Arithmatic Tester
     * @param args
     */
    public static void main(String[] args) {

        Calculator calc1 = new Calculator();
        Calculator calc2 = new Calculator();
        Calculator calc3 = new Calculator();
        Calculator calc4 = new Calculator();
        Calculator calc5 = new Calculator();
        Calculator calc6 = new Calculator();
        
        System.out.println("Testing Calculator...\n");
        
        //Test 1
        System.out.println("Test 1: Calculate 1 + 2");
        System.out.println("Expected: 3");
        calc1.calculate(1);
        calc1.setLastOperator("+");
        calc1.calculate(2);
        calc1.getResult();
        System.out.println(calc1.toString());
    
        
        //Test 2
        System.out.println("\nTest 2: Calcualte 1 + 2 + 3");
        System.out.println("Expected: 6");
        calc2.calculate(1);
        calc2.setLastOperator("+");
        calc2.calculate(2);
        calc2.getLastOperator();
        calc2.calculate(3);
        System.out.println(calc2.toString());
    
        //Test 3
        System.out.println("\nTest 3: Calcualte 1 + 2 * 3");
        System.out.println("Expected: 9");
        calc3.calculate(1);
        calc3.setLastOperator("+");
        calc3.calculate(2);
        calc3.setLastOperator("*");
        calc3.calculate(3);
        System.out.println(calc3.toString());
        
        //Test 4
        System.out.println("\nTest 4: Calcualte 8 / 2");
        System.out.println("Expected: 4");
        calc4.calculate(8);
        calc4.setLastOperator("/");
        calc4.calculate(2);
        System.out.println(calc4.toString());
        
        //Test 5
        System.out.println("\nTest 5: Calcualte 4 - 2");
        System.out.println("Expected: 2");
        calc5.calculate(4);
        calc5.setLastOperator("-");
        calc5.calculate(2);
        System.out.println(calc5.toString());
        
        //Test 6
        System.out.println("\nTest 6: Calcualte -2 + 2");
        System.out.println("Expected: 0");
        calc6.calculate(-2);
        calc6.setLastOperator("+");
        calc6.calculate(2);
        System.out.println(calc6.toString());
    }
}