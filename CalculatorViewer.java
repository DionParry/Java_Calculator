/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.Color;
import javax.swing.WindowConstants;

/**
 * Title: Java Calculator - Lab6
 * @author Dion
 * 13/11/2016
 */
public class CalculatorViewer {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        CalculatorFrame frame = new CalculatorFrame();
        frame.setJMenuBar(new MenuBar()); //calls Menu
        frame.setSize(360, 260);
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setTitle("Calculator");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
