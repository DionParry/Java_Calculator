/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Title: Java Calculator - Lab6
 * @author Dion
 * 13/11/2016
 */
public class Calculator {

    public double result;        // Holds accumulator value
    public String lastOperator;  // Stores last operator entered

    public Calculator() {
        result = 0;
        lastOperator = "=";
    }

    /**
     *
     * @return result
     */
    public double getResult() {
        return result;
    }

    /**
     *
     * @return lastOperator
     */
    public String getLastOperator() {
        return lastOperator;
    }

    /**
     *
     * @param operator
     */
    public void setLastOperator(String operator) {
        lastOperator = operator;
    }

    /**
     * Carries out the pending calculation.
     *
     * @param x the value to be accumulated with the prior result.
     * @return 
     */
    public double calculate(double x) {
        if (lastOperator.equals("+")) {
            result += x;
        } else if (lastOperator.equals("-")) {
            result -= x;
        } else if (lastOperator.equals("*")) {
            result *= x;
        } else if (lastOperator.equals("/")) {
            result /= x;
        } else if (lastOperator.equals("1/x")) { //reprecidol
            result = 1 / x;
        } else if (lastOperator.equals("=")) {
            result = x;
        } else if (lastOperator.equals("\u221A")) { //square root
            result = Math.sqrt(x);
        } else if (lastOperator.equals("%")) { //check this one (percentage)
            result = Math.floor(x * 100) / 100;
        } else if (lastOperator.equals("+/-")) {    
            if ((x % 2) == 0) {
                result = -x;
            } else {
                result = x;      
            }
        }
        //System.out.println("result = " + result);
        return result;
    }

    /**
     * Returns string representation of calculator state.
     *
     * @param return calculator state
     * @return 
     */
    public String toString() {
        return getClass().getSimpleName()
                + "[result = " + result + ", lastOperator = " + lastOperator + "]";
    }
}
